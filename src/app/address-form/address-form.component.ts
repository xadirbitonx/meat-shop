import { Component } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {UserService} from "../../services/user.service";
import {IAddress} from "../../models/types";

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.less']
})
export class AddressFormComponent {
  public userAddress: IAddress = {address: "", addressNumber: null, city: ""};

  constructor(private userService: UserService) {
    if (userService.user.address) {
      this.userAddress = userService.user.address;
    }
  }
}
