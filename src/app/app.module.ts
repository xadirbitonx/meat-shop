import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule, RoutingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon'
import {MatButtonModule} from '@angular/material/button';
import {CartService} from "../services/cart.service";
import {ProductsByCategoryComponent} from './products-by-category/products-by-category.component';
import {MatDialogModule} from "@angular/material/dialog";
import {ProductDialogComponent} from './product-dialog/product-dialog.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatRadioModule} from "@angular/material/radio";
import {PhoneVerificationDialogComponent} from './phone-verification-dialog/phone-verification-dialog.component';
import {UserService} from "../services/user.service";
import {AddressFormComponent} from './address-form/address-form.component';
import {FetcherService} from "../services/fetcher.service";
import {HttpConfigInterceptor} from "./interceptors/httpconfig.interceptor";
import {LoginComponent} from './login/login.component';
import {MakeOrderComponent} from './make-order/make-order.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    ProductsByCategoryComponent,
    ProductDialogComponent,
    PhoneVerificationDialogComponent,
    AddressFormComponent,
    LoginComponent,
    MakeOrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatTooltipModule
  ],
  providers: [CartService, UserService, FetcherService, {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpConfigInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private iconRegister: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.registerIcon("close");
    this.registerIcon("cart");
    this.registerIcon("home");
    this.registerIcon("delete");
    this.registerIcon("credit-card");
  }

  private registerIcon(iconName: string): void {
    this.iconRegister.addSvgIcon(iconName, this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/' + iconName + '.svg'));
  }
}
