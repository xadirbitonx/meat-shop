import {Component} from '@angular/core';
import {CartService} from "../../services/cart.service";
import {ClientSideProduct} from "../../models/types";
import {MatDialog} from "@angular/material/dialog";
import {PhoneVerificationDialogComponent} from "../phone-verification-dialog/phone-verification-dialog.component";
import {UserService} from "../../services/user.service";
import {dialogConfig} from "../../services/utils";
import {MakeOrderComponent} from "../make-order/make-order.component";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent {
  public cart: { [id: string]: ClientSideProduct; };
  public productsArray: ClientSideProduct[] = [];

  constructor(private cartService: CartService,
              private userService: UserService,
              private dialog: MatDialog) {
    this.cart = this.cartService.cart;
    this.initializeProductsArray();
  }

  public removeFromCart(product: ClientSideProduct) {
    this.cartService.removeFromCart(product);
    this.initializeProductsArray();
  }

  private initializeProductsArray() {
    this.productsArray = [];
    for (const key in this.cart) {
      const product: ClientSideProduct = this.cart[key];
      this.productsArray.push(product);
    }
  }

  public getTotalPrice(): number {
    let sum = 0;
    for (const product of this.productsArray) {
      sum += product.selectedWeight * product.priceForKilo;
    }
    return sum;
  }

  public makeOrder(): void {
    if (!this.userService.isConnected()) {
      this.openPhoneVerificationDialog();
    } else {
      this.openMakeOrderDialog();
    }
  }

  private openPhoneVerificationDialog(): void {
    this.dialog.open(PhoneVerificationDialogComponent, dialogConfig);
  }

  private openMakeOrderDialog(): void {
    this.dialog.open(MakeOrderComponent, dialogConfig);
  }
}
