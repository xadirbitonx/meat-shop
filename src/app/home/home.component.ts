import {Component, OnInit} from '@angular/core';
import {Category, ServerSideProduct} from "../../models/types";
import {FetcherService} from "../../services/fetcher.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  public allProducts: ServerSideProduct[] = [];
  public beefProducts: ServerSideProduct[] = [];
  public gooseProducts: ServerSideProduct[] = [];

  constructor(private fetcher: FetcherService) {
  }

  ngOnInit(): void {
    this.getAllProducts();
  }

  private async getAllProducts(): Promise<void> {
    this.allProducts = await this.fetcher.get("product/getAllProducts");
    this.beefProducts = this.allProducts.filter((product: ServerSideProduct) => product.category == Category.Beef);
    this.gooseProducts = this.allProducts.filter((product: ServerSideProduct) => product.category == Category.Goose);
  }
}
