import {Component} from '@angular/core';
import {UserService} from "../../services/user.service";
import {PhoneVerificationDialogComponent} from "../phone-verification-dialog/phone-verification-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {dialogConfig} from "../../services/utils";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent {

  constructor(private userService: UserService,
              private dialog: MatDialog) {
  }

  public getUserName(): string {
    return this.userService.user.firstName ? this.userService.user.firstName : "אורח";
  }

  public login(): void {
    this.openPhoneVerificationDialog();
  }

  public logout(): void {
    localStorage.removeItem("token");
    this.userService.user = {};
  }

  private openPhoneVerificationDialog(): void {
    this.dialog.open(PhoneVerificationDialogComponent, dialogConfig);
  }

  public isConnected(): boolean {
    return this.userService.isConnected();
  }
}
