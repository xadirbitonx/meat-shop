import { Component } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-make-order',
  templateUrl: './make-order.component.html',
  styleUrls: ['./make-order.component.less']
})
export class MakeOrderComponent {
  public deliveryMode: DeliveryMode = DeliveryMode.chooseAlready;
  public didUserConfirmOrder = false;

  constructor(public dialogRef: MatDialogRef<MakeOrderComponent>) { }

  public cancel(): void {
    this.dialogRef.close();
  }

  public userConfirm(): void {
    this.didUserConfirmOrder = !this.didUserConfirmOrder;
  }

  public makeOrder(): void {
    if (this.didUserConfirmOrder) {
      this.dialogRef.close();
    }
  }
}

enum DeliveryMode {
  delivery = 0,
  takeOut = 1,
  chooseAlready = 999
}
