import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {UserService} from "../../services/user.service";
import {FetcherService} from "../../services/fetcher.service";
import {IUser} from "../../models/types";

@Component({
  selector: 'app-phone-verification-dialog',
  templateUrl: './phone-verification-dialog.component.html',
  styleUrls: ['./phone-verification-dialog.component.less']
})
export class PhoneVerificationDialogComponent {
  public phoneNumber: string = "";
  public firstName: string = "";
  public lastName: string = "";
  public verificationCode: string = "";
  public viewMode: ViewMode = ViewMode.enterPhoneNumber;

  constructor(public dialogRef: MatDialogRef<PhoneVerificationDialogComponent>,
              private userService: UserService,
              private fetcher: FetcherService) {
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public async sendVerificationCode(): Promise<void> {
    if (this.isPhoneNumberValid()) {
      await this.fetcher.post("sendVerificationCode", {phone: this.phoneNumber});
      this.viewMode = ViewMode.verifyCode;
    }
  }

  public async verifyCode(): Promise<void> {
    const userAndToken: { user: IUser, token: string } = await this.fetcher.post("verifyCode", {
      phone: this.phoneNumber,
      code: this.verificationCode
    });

    localStorage.setItem("token", userAndToken.token);
    this.userService.user = userAndToken.user;

    if (this.userService.isFirstTimeLoggingIn()) {
      this.viewMode = ViewMode.enterName;
    } else {
      this.dialogRef.close();
    }
  }

  public isPhoneNumberValid(): boolean {
    return this.userService.isPhoneNumberValid(this.phoneNumber);
  }

  public async updateUserName(): Promise<void> {
    if (this.firstName.length > 0 && this.lastName.length > 0) {
      const user: IUser = {firstName: this.firstName, lastName: this.lastName};
      await this.userService.updateUserDetails(user);
      this.dialogRef.close();
    }
  }
}

enum ViewMode {
  enterPhoneNumber = 0,
  verifyCode = 1,
  enterName = 2
}
