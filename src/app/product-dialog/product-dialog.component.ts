import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CartService} from "../../services/cart.service";
import {ClientSideProduct, ServerSideProduct, SlicingOption, TrayDivision} from "../../models/types";
import {getImage} from "../../services/utils";

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.less']
})
export class ProductDialogComponent {
  public selectedWeight: number = 1;
  public trayDivision = TrayDivision;
  public selectedSliceOption: SlicingOption | null = null;
  public selectedTrayDivision: TrayDivision | null = null;

  constructor(public dialogRef: MatDialogRef<ProductDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public product: ServerSideProduct,
              private cartService: CartService) {
    product.image = getImage(product.image);
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public increaseSelectedWeight(): void {
    if (this.selectedWeight !== 10) {
      this.selectedWeight += 0.5;
    }
  }

  public decreaseSelectedWeight(): void {
    if (this.selectedWeight !== 0.5) {
      this.selectedWeight -= 0.5;
    }
  }

  public addToCart(): void {
    if (this.selectedSliceOption && this.selectedTrayDivision) {
      const product = new ClientSideProduct(this.product, this.selectedWeight, this.selectedSliceOption, this.selectedTrayDivision);
      this.cartService.addToCart(product);
      this.dialogRef.close(product);
    }
  }
}
