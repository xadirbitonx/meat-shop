import {Component, Input, OnInit} from '@angular/core';
import {ServerSideProduct} from "../../models/types";
import {getImage} from "../../services/utils";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ProductDialogComponent} from "../product-dialog/product-dialog.component";

@Component({
  selector: 'app-products-by-category',
  templateUrl: './products-by-category.component.html',
  styleUrls: ['./products-by-category.component.less']
})
export class ProductsByCategoryComponent implements OnInit {
  @Input() title: string = "";
  @Input() image: string = "";
  @Input() products: ServerSideProduct[] = [];
  private config: MatDialogConfig<ServerSideProduct>;
  public isExpanded = false;
  public buttonText: string = "הרחב";

  constructor(private dialog: MatDialog) {
    this.config = new MatDialogConfig<ServerSideProduct>();
    this.config.width = "550px";
    this.config.panelClass = "dialog";
  }

  ngOnInit(): void {
    if (this.image.length > 0) {
      this.image = getImage(this.image);
    }
  }

  openProductDialog(product: ServerSideProduct): void {
    this.config.data = product;
    this.dialog.open(ProductDialogComponent, this.config);
  }

  public getImage(imageName: string): string {
    return getImage(imageName);
  }

  public toggleView(): void {
    this.isExpanded = !this.isExpanded;
    if (this.isExpanded) {
      this.buttonText = "הקטן";
    } else {
      this.buttonText = "הרחב";
    }
  }
}
