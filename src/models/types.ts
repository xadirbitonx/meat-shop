export class ClientSideProduct {
  id: string;
  name: string;
  priceForKilo: number;
  category: Category;
  image: string;
  selectedWeight: number;
  slicingOption: SlicingOption;
  trayDivision: TrayDivision;

  constructor(product: ServerSideProduct, selectedWeight: number, slicingOption: SlicingOption, trayDivision: TrayDivision) {
    this.id = product._id;
    this.name = product.name;
    this.priceForKilo = product.priceForKilo;
    this.category = product.category;
    this.image = product.image;
    this.selectedWeight = selectedWeight;
    this.slicingOption = slicingOption;
    this.trayDivision = trayDivision;
  }
}

export interface ServerSideProduct {
  _id: string;
  name: string;
  priceForKilo: number;
  category: Category;
  image: string;
  slicingOptions: SlicingOption[];
}

export interface IAddress {
  address: string,
  addressNumber: number | null,
  city: string
}

export interface IUser {
  phoneNumber?: string,
  firstName?: string,
  lastName?: string,
  address?: IAddress
}

export enum SlicingOption {
  Slices = "פרוסות",
  Cubes = "קוביות",
  strips = "רצועות",
  ground = "טחון",
  None = "שלם"
}

export enum TrayDivision {
  Small = "חצי קילו",
  Big = "קילו"
}

export enum Category {
  Beef = "בקר",
  Lamb = "טלה",
  Chicken = "עופות",
  Goose = "אווז"
}
