import { Injectable } from '@angular/core';
import {ClientSideProduct} from "../models/types";

@Injectable()
export class CartService {

  public cart: { [id: string] : ClientSideProduct; } = {};
  constructor() { }

  public addToCart(product: ClientSideProduct): void {
    this.cart[product.id] = product;
  }

  public removeFromCart(product: ClientSideProduct): void {
    delete this.cart[product.id];
  }
}
