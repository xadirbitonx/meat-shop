import {Injectable} from '@angular/core';
import {IUser} from "../models/types";
import {FetcherService} from "./fetcher.service";

@Injectable()
export class UserService {
  public user: IUser = {};
  private regex = new RegExp('[0-9]{10}');

  constructor(private fetcher: FetcherService) {
    this.getUserFromServer();
  }

  public async getUserFromServer(): Promise<void> {
    try {
      this.user = await this.fetcher.get("getUser");
    } catch (e) {
      localStorage.removeItem("token");
      console.log(e);
    }
  }

  public isPhoneNumberValid(number: string | undefined): boolean {
    return number !== undefined && this.regex.test(number);
  }

  public isAddressValid(): boolean {
    return true;
  }

  public isConnected(): boolean {
    return this.isPhoneNumberValid(this.user.phoneNumber) && localStorage.getItem("token") !== null;
  }

  public isFirstTimeLoggingIn(): boolean {
    return this.isConnected() && !this.hasName();
  }
  private hasName(): boolean {
    return this.user.firstName !== undefined && this.user.lastName !== undefined;
  }

  public async updateUserDetails(user: IUser): Promise<void> {
    user.phoneNumber = this.user.phoneNumber;
    this.user = await this.fetcher.post("updateUserDetails", {user: user});
  }
}
