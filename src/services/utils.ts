import {MatDialogConfig} from "@angular/material/dialog";

export function getImage(imageName: string): string {
  if (!imageName.includes(".jpg")) {
    imageName += ".jpg"
  }
  return "../../assets/images/" + imageName;
}

export const dialogConfig: MatDialogConfig<void> = new MatDialogConfig<void>();
dialogConfig.width = "550px";
dialogConfig.panelClass = "dialog";
